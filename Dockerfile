FROM maven:3.6.3-jdk-11

WORKDIR /tmp/test
ENTRYPOINT ["/bin/sh"]
CMD ["-c", "mvn clean install serenity:aggregate -Dtoken1=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 -Dtoken2=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"]
