# Rebels Assessment Task #

Test Automation framework for Rebels's QA Test Automation assignment created specifically for company recruitment process. 
Some tests for GitHub Gists API have been automated. However, the project provides an example of the architecture of a test automation framework to test API in a BDD framework implemented using Cucumber, Serenity and Rest Assured.

## Overview ##
This test framework includes api tests for GitHub Gists API. The framework is based on BDD achieved using Rest Assured library to handle the API tests:<br>
        https://developer.github.com/v3/gists/
## Test Scenarios ##
All test Scenarios are here: src/main/resources/Feature in the files GistsCRUD.feature, PrivateGistsAccessibility.feature
Several scenarios were skipped by tag @ignore, because of the found bugs.

## How To Run ##
        In case you have acess to https://gitlab.com/protsenko.dv/rebelstesttask/-/pipelines
        1. go to CI/CD press "Run pipeline" button

        In case you have Linux and installed Docker on your local machine:
        1. clone the project
        2. go to the project directory
        3. execute shell script -  run.sh
                ./run.sh
                
        In case you have Windows and installed Docker:
        1. go to the project directory
        2. Find the script run.sh, open it
        3. Please comment (by #) the row: docker run --user=`id -u` -v `pwd`:/tmp/test -it testrunner:local
        4. Please uncomment row: docker run -v ${pwd}:/tmp/test -it testrunner:local
        5. save the file
        6. run script ./run.sh
        
        In case you have Linux and installed java 8 (and higher) and maven 3:
        1. go to the project directory
        2. run command: 
            mvn clean install serenity:aggregate -Dtoken1=<git_hub_token1> -Dtoken2=<git_hub_token2>

## GitHub Tokens ##
For correct work of the tests the github tokens are specified as environment variables. You can generate your own or take the generated in Dockerfile

## Configuration file ##
Please find the configuration file in the project directory: data.properties

## Reporting ##
The reporting tool is serenity.
After the running of tests please find the report:

        In case you are on https://gitlab.com/protsenko.dv/rebelstesttask/-/pipelines
        1. Open last build
        2. Press to "deploy" button
        3. Browse the job artifact
        4. Open the target/site/serenity/index.html
        5. accept the unsecure connection

        In case you run tests on your local machine:
        1. open target/site/serenity/index.html

## Logs ##
After the running of tests, please find the logs in the project directory: *logs*


# Tools Used
Programming Language - Java<br/>
Framework Details - BDD using Cucumber and Gherkin Syntax, Rest-assured<br/>
Reporting - Serenity<br/>
JSON libraries - for manipulating JSON data<br/>

# Had to do
Given:
• API server: pick any > https://any-api.com/
• CI platform: GitLab
• BDD: Cucumber/Gherkin
• Framework: Java Serenity

Need:
• Automate the endpoints inside the CI pipeline to test the service
• Cover positive and negative scenarios
• Write instructions in Readme file on how to install, run and write new tests
• Document the endpoints interactions
• Set Html reporting tool with the test results