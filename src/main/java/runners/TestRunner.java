package runners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = {
                "src/main/resources/Feature/GistsCRUD.feature",
                "src/main/resources/Feature/PrivateGistsAccessibility.feature"
        }
        , glue = "stepdefinitions"
)

public class TestRunner {

    @BeforeClass
    public static void setup() {
        try (PrintStream printStream = new PrintStream(new FileOutputStream(System.getProperty("user.dir")
                + "/logs/test.log", false), false)) {
            System.setOut(printStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


}
