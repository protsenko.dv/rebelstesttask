package managers;

import io.restassured.RestAssured;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import static java.lang.String.format;
import static java.lang.System.getProperty;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class ConfigFileManager {

    private static final Logger log = LogManager.getLogger(ConfigFileManager.class);
    private static final String DEFAULT_API_URI = "api.uri";
    public static final String PROPERTIES_PATH = "data.properties";
    public static final String GISTS_API = "gists.api";
    public static final String SEARCH_API = "search.api";
    public static final String USERS_API = "users.api";
    public static final String CURRENT_USERS_API = "current.user.api";



    public static Properties loadProperties() {
        Properties properties = null;
        try (BufferedReader reader = new BufferedReader(new FileReader(PROPERTIES_PATH))) {
            properties = new Properties();
            properties.load(reader);
        } catch (IOException e) {
            log.error("Not able to Load property file !!");
            e.printStackTrace();
        }
        return properties;
    }

    public static String getPropertyValueByName(String name) {
        return loadProperties().getProperty(name);
    }

    public static String getDefaultApiUri() {
        return getParam("apiUri", getPropertyValueByName(DEFAULT_API_URI));
    }

    public static String getTestUserToken(String userName) {
        switch (userName) {
            case "usertest1":
                return getMandatoryEnvVariable("token1");
            case "usertest2":
                return getMandatoryEnvVariable("token2");
            case "anonymous":
                return null;
            default:
                throw new IllegalArgumentException(format("Unknown userName [%s], User name can be only usertest1,"
                                + " usertest2 OR anonymous"
                        , userName));
        }
    }

    public static String getMandatoryEnvVariable(String name) {
        String parameter = getProperty(name);
        if (isBlank(parameter)) {
            throw new RuntimeException("Failed to detect:" + name);
        }
        return parameter;
    }

    static {
        RestAssured.baseURI = getDefaultApiUri();
        log.info("Set Base URI to =" + RestAssured.baseURI);
    }

    public static String getParam(String variableName, String defaultValue) {
        String env = System.getenv(variableName);
        if (env != null) {
            variableName = defaultValue;
        }
        String result = System.getProperty(variableName, defaultValue);
        if (result == null) {
            result = defaultValue;
        }
        return result;
    }

}

