package stepdefinitions;

import cucumber.api.java.en.When;
import general.SearchApi;

import static managers.ConfigFileManager.getTestUserToken;

public class StepDefinitionSearch extends SearchApi {

    @When("User \"(.*)\" sent a \"(.*)\" Search Api requests")
    public void userSentASearchRequest(String userName, int amount) {
        sendRandomSearchRequests(getTestUserToken(userName), amount);
    }


}
