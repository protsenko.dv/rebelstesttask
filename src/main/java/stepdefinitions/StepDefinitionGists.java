package stepdefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import general.GistsApi;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;

import java.util.List;

import static java.util.Arrays.stream;
import static managers.ConfigFileManager.getTestUserToken;
import static org.junit.Assert.*;
import static utils.CustomAsserts.assertEventually;
import static utils.ValuesStorage.getCreatedGistId;

public class StepDefinitionGists extends GistsApi {
    private static final Logger log = LogManager.getLogger(StepDefinitionGists.class);

    @After
    public void after(Scenario scenario) {
        log.info("Scenario \"" + scenario.getName() + "\" is finished with the status: " + scenario.getStatus());
    }

    @Given("^no gists with names \"(.*)\" under user \"(.*)\"$")
    public void noGistUnderUser(String gistNames, String userName) {
        stream(gistNames.split(","))
                .forEach(gistName -> deleteUsersGistsByName(getTestUserToken(userName), userName, gistName));
    }

    @When("^User \"(.*)\" deletes gists with names \"(.*)\"$")
    public void deleteGistForUser(String userName, String gistNames) {
        noGistUnderUser(gistNames, userName);
    }

    @When("^User \"(.*)\" creates a public gist \"(.*)\"$")
    public void whenUserCreatesPublicGist(String userName, String gistName) {
        createGistWithName(getTestUserToken(userName), true, gistName);
    }

    @When("^User \"(.*)\" creates a private gist \"(.*)\"$")
    public void whenUserCreatesPrivateGist(String userName, String gistName) {
        createGistWithName(getTestUserToken(userName), false, gistName);
    }

    @When("^User \"(.*)\" creates a gist \"(.*)\" without \"public\" parameter$")
    public void whenUserCreatesGistWithout(String userName, String gistName) {
        createGistWithNameWithoutPublic(getTestUserToken(userName), gistName);
    }

    @When("^User \"(.*)\" creates a \"(.*)\" gist with name \"(.*)\" and saves Id to storage$")
    public void userCreatesGistAndStoreId(String userName, String isPublic, String gistName) {
        createGistAndSaveIdToValueStorage(getTestUserToken(userName), isPublic.equalsIgnoreCase("public"), gistName);
    }

    @Then("^Validation Failed when the user \"(.*)\" creates a gist without \"files\" parameter$")
    public void validationFailedWhenCreatesGistWithoutFiles(String userName) {
        Response response = createGistWithoutFiles(getTestUserToken(userName));
        assertEquals("The validation is not failed. Response code is " + response.statusCode(), 422,
                response.statusCode());
    }

    @Then("^Validation Failed when the user \"(.*)\" creates a gist without \"content\" parameter$")
    public void validationFailedWhenCreatesGistWithoutContent(String userName) {
        Response response = createGistWithoutContent(getTestUserToken(userName));
        assertEquals("The validation is not failed. Response code is " + response.statusCode(), 422,
                response.statusCode());
    }

    @Then("^Validation Failed when the user \"(.*)\" creates a gist with empty \"content\" parameter$")
    public void validationFailedWhenCreatesGistWithEmptyContent(String userName) {
        Response response = createGistEmptyContent(getTestUserToken(userName));
        assertEquals("The validation is not failed. Response code is " + response.statusCode(), 422,
                response.statusCode());
    }

    @Then("^Validation Failed when the user \"(.*)\" edits a gist \"(.*)\" to empty \"content\" parameter$")
    public void validationFailedWhenEditsGistWithEmptyContent(String userName, String gistName) {
        Response response = editGistFileContent(getTestUserToken(userName), getCreatedGistId().get(gistName), " ");
        assertEquals("The validation is not failed. Response code is " + response.statusCode(), 422,
                response.statusCode());
    }

    @When("^User \"(.*)\" edits a name of gist \"(.*)\" from storage to new gist name \"(.*)\"$")
    public Response userEditsGistNameFromStorage(String userName, String oldGistName, String newGistName) {
        return editGistFileName(getTestUserToken(userName),
                getCreatedGistId().get(oldGistName), newGistName);
    }

    @When("^User \"(.*)\" edits a content of gist \"(.*)\" from storage to new gist content \"(.*)\"$")
    public Response userEditsGistContentFromStorage(String userName, String gistName, String newGistContent) {
        return editGistFileContent(getTestUserToken(userName), getCreatedGistId().get(gistName), newGistContent);
    }

    @Then("^User \"(.*)\" successfully edited a name of gist \"(.*)\" from storage to new gist name \"(.*)\"$")
    public void userSuccessfullyEditsGistNameFromStorage(String userName, String oldGistName, String newGistName) {
        Response response = userEditsGistNameFromStorage(userName, oldGistName, newGistName);
        assertEquals("Edition of gist unsuccessful: code " + response.statusCode(), 200, response.statusCode());
        assertTrue("The gist name still old", new JSONObject(getGistById(getTestUserToken(userName),
                getCreatedGistId().get(oldGistName)).body().asString()).getJSONObject("files").has(newGistName));
    }

    @Then("^User \"(.*)\" successfully edited a content of gist \"(.*)\" from storage to new gist content \"(.*)\"$")
    public void userSuccessfullyEditsGistContentFromStorage(String userName, String gistName, String newGistContent) {
        Response response = userEditsGistContentFromStorage(userName, gistName, newGistContent);
        assertEquals("Edition of gist unsuccessful: code " + response.statusCode(), 200,
                response.statusCode());
        assertEquals("The gist content still old", newGistContent, new JSONObject(
                getGistById(getTestUserToken(userName), getCreatedGistId().get(gistName)).body().asString())
                .getJSONObject("files")
                .getJSONObject(gistName).getString("content"));
    }

    @Given("^a \"(.*)\" gist \"(.*)\" under user \"(.*)\"$")
    public void givenPublicGistUnderUser(String publicity, String gistName, String userName) {
        boolean isPublic = publicity.equalsIgnoreCase("public");
        deleteUsersGistsByName(getTestUserToken(userName), userName, gistName);
        createGistWithName(getTestUserToken(userName), isPublic, gistName);
    }

    @Then("^the user \"(.*)\" can see the gist of the target user \"(.*)\" with gistName \"(.*)\"$")
    public void theGistIsDisplayed(String loggedUser, String targetUser, String gistName) {
        assertEventually(3, 500, () ->
                assertFalse(getUsersGistsByName(getTestUserToken(loggedUser), getUserName(getTestUserToken(targetUser)),
                gistName).isEmpty()));
    }


    @Then("^the user \"(.*)\" can't see the gist of the target user \"(.*)\" with gistName \"(.*)\"$")
    public void theGistIsNotDisplayed(String loggedUser, String targetUser, String gistName) {
        assertEventually(3, 500, () ->
                assertTrue(getUsersGistsByName(getTestUserToken(loggedUser), targetUser, gistName).isEmpty()));
    }

    @When("^The user \"(.*)\" make the private gist \"(.*)\" public$")
    public void iMakeThePrivateGistPublic(String userName, String gistName) {
        makeGistsPublic(getTestUserToken(userName), userName, gistName);
    }

    @Then("^user \"(.*)\" can see updated in \"(.*)\" seconds by method Show All Gists the gists:$")
    public void seeAllGistsTheCreatedInHour(String loggedUser, int seconds, List<String> listOfGists) {
        String since = new DateTime().toDateTime(DateTimeZone.UTC).minusSeconds(seconds).toString();
        listOfGists.forEach(gistName -> getAllGists(getTestUserToken(loggedUser), since)
                .forEach(gist -> gist.getJSONObject("files").has(gistName))
        );
    }

    @When("User \"(.*)\" sent a \"(.*)\" gistApi requests")
    public void userSentAGistRequest(String userName, int amount) {
        sendRandomGistRequests(getTestUserToken(userName), amount);
    }


    @Then("gistAPI rate limit exceeded for user \"(.*)\"")
    public void anyNewGistApiRequestsItUnsuccessfulForUser(String userName) {
        assertTrue("API rate limit is not exceeded", limitOfGistsRequestsExceeded(getTestUserToken(userName)));
    }

    @Then("^the system returns Requires authentication error when user \"(.*)\" tries create a gist$")
    public void errorWhenUserTriesCreateAGist(String userName) {
        assertTrue("The gist is created without correct authtoken",
                isRequiredAuthenticationForGistCreating(getTestUserToken(userName)));
    }

    @Then("^the system returns Requires authentication error when user \"(.*)\" tries edit a gist \"(.*)\"$")
    public void errorWhenUserTriesEditAGistOfUser(String loggedUserName, String gistName) {
        assertTrue("The gist is edited without correct authtoken",
                isRequiredAuthenticationForGistEditing(getTestUserToken(loggedUserName), getCreatedGistId().get(gistName)));
    }

    @Then("^the system returns Requires authentication error when user \"(.*)\" tries delete a gist \"(.*)\"$")
    public void errorWhenUserTriesDeleteAGistOfUser(String loggedUserName, String gistName) {
        assertTrue("The gist is deleted without correct authtoken",
                isRequiredAuthenticationForGistDeletion(getTestUserToken(loggedUserName), getCreatedGistId().get(gistName)));
    }


}
