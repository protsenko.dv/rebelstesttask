package general;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.ValuesStorage;

import java.util.List;
import java.util.stream.Collectors;

import static managers.ConfigFileManager.*;
import static utils.ValuesStorage.setGistValueStorage;

public class GistsApi extends AbstractApi {

    private static final Logger logger = LogManager.getLogger(GistsApi.class);

    public GistsApi() {
        setGistValueStorage();
    }

    protected Response getGistById(String authToken, String gistId) {
        String uri = getPropertyValueByName(GISTS_API) + "/" + gistId;
        logger.info(uri);
        return authToken == null ? get(uri) : get(authToken, uri);
    }

    public List<JSONObject> getUsersGists(String authToken, String targetUserName) {
        String uri = getPropertyValueByName(USERS_API)
                + "/" + targetUserName
                + getPropertyValueByName(GISTS_API);
        logger.info(uri);
        Response response = authToken == null ? get(uri) : get(authToken, uri);
        JSONArray gistsOfUser = new JSONArray(response.body().asString());
        return jsonArrayToList(gistsOfUser);
    }

    public List<JSONObject> getAllGists(String authToken, String timeStamp) {
        String uri = getPropertyValueByName(GISTS_API)
                + "/public?since=" + timeStamp;
        logger.info(uri);
        Response response = authToken == null ? get(uri) : get(authToken, uri);
        JSONArray gistsOfUser = new JSONArray(response.body().asString());
        return jsonArrayToList(gistsOfUser);
    }

    public List<JSONObject> getUsersGistsByName(String authToken, String targetUserName, String gistName) {
        return getUsersGists(authToken, targetUserName).stream().filter(gist ->
                gist.getJSONObject("files")
                        .has(gistName)).collect(Collectors.toList());
    }

    protected void deleteUsersGistsByName(String authToken, String targetUserName, String gistName) {
        getUsersGistsByName(authToken, targetUserName, gistName)
                .forEach(gist -> deleteUsersGistById(authToken,
                        gist.getString("id")));
    }

    protected Response deleteUsersGistById(String authToken, String gistId) {
        String uri = getPropertyValueByName(GISTS_API) + "/" + gistId;
        logger.info(uri);
        return authToken == null ? delete(uri) : delete(authToken, uri);
    }

    public Response createGistWithName(String authToken, boolean isPublic, String gistName) {
        JSONObject json = new JSONObject().put("public", isPublic)
                .put("files", new JSONObject().put(gistName, new JSONObject().put("content", "123")));
        return createGist(authToken, json);
    }

    public void createGistWithNameWithoutPublic(String authToken, String gistName) {
        JSONObject json = new JSONObject().put("files", new JSONObject().put(gistName, new JSONObject().put("content", "123")));
        createGist(authToken, json);
    }

    public Response createGistWithoutFiles(String authToken) {
        JSONObject json = new JSONObject().put("public", true).put("description", "description");
        return createGist(authToken, json);
    }

    public Response createGistWithoutContent(String authToken) {
        JSONObject json = new JSONObject().put("files", new JSONObject().put("withoutContent", new JSONObject()));
        return createGist(authToken, json);
    }

    public Response createGistEmptyContent(String authToken) {
        JSONObject json = new JSONObject().put("files", new JSONObject().put("emptyContent", new JSONObject().put("content", " ")));
        return createGist(authToken, json);
    }

    private Response createGist(String authToken, JSONObject json) {
        String uri = getPropertyValueByName(GISTS_API);
        return authToken == null ? post(uri, json) : post(authToken, uri, json);
    }

    public void createGistAndSaveIdToValueStorage(String authToken, boolean isPublic, String gistName) {
        Response response = createGistWithName(authToken, isPublic, gistName);
        ValuesStorage.getCreatedGistId().put(gistName,
                new JSONObject(response.body().asString()).getString("id"));
    }

    public void makeGistsPublic(String authToken, String userName, String gistName) {
        getUsersGistsByName(authToken, userName, gistName).forEach(gist -> {
            gist.put("public", true);
            patch(authToken, getPropertyValueByName(GISTS_API), gist);
        });
    }

    public Response editGistFileName(String authToken, String gistId, String newGistName) {
        Response gist = getGistById(authToken, gistId);
        JSONObject oldJson = new JSONObject(gist.body().asString());
        JSONObject files = oldJson.getJSONObject("files");

        String oldGistName = oldJson.getJSONObject("files").keys().next();
        JSONObject file = files.getJSONObject(oldGistName);

        JSONObject newJson = oldJson.put("description", "edited").put("files",
                files.put(oldGistName,
                        file.put("filename",
                                newGistName)));

        return editGistFile(authToken, gistId, newJson);
    }

    public Response editGistFileContent(String authToken, String gistId, String newGistContent) {
        Response gist = getGistById(authToken, gistId);
        JSONObject oldJson = new JSONObject(gist.body().asString());
        JSONObject files = oldJson.getJSONObject("files");

        String oldGistName = oldJson.getJSONObject("files").keys().next();
        JSONObject file = files.getJSONObject(oldGistName);

        JSONObject newJson = oldJson.put("description", "edited").put("files",
                files.put(oldGistName,
                        file.put("content",
                                newGistContent)));

        return editGistFile(authToken, gistId, newJson);
    }

    public Response editGistFile(String authToken, String gistId, JSONObject newJson) {
        String uri = getPropertyValueByName(GISTS_API) + "/" + gistId;
        return authToken == null ? patch(uri, newJson) : patch(authToken, uri, newJson);
    }

    public Response sendRandomGistRequest(String authToken) {
        String uri = getPropertyValueByName(USERS_API)
                + "/octocat" //github test-user
                + getPropertyValueByName(GISTS_API);
        logger.info(uri);
        return authToken == null ? get(uri) : get(authToken, uri);
    }

    public void sendRandomGistRequests(String authToken, int amount) {
        for (int i = 0; i < amount; i++) {
            sendRandomGistRequest(authToken);
        }
    }

    public Boolean limitOfGistsRequestsExceeded(String authToken) {
        Response response = sendRandomGistRequest(authToken);
        logger.info(response.body().asString());
        return response.statusCode() == 403 && new JSONObject(response.body().asString())
                .getString("message").contains("API rate limit exceeded");
    }

    public Boolean isRequiredAuthenticationForGistCreating(String authToken) {
        Response response = createGistWithName(authToken, true, "This Gist should not be created");
        logger.info(response.body().asString());
        return response.statusCode() == 401 && new JSONObject(response.body().asString())
                .getString("message").contains("Requires authentication");
    }

    public Boolean isRequiredAuthenticationForGistEditing(String authToken, String gistId) {
        Response response = editGistFileName(authToken, gistId, "This Gist should not be edited");
        logger.info(response.body().asString());
        return response.statusCode() == 404 && new JSONObject(response.body().asString())
                .getString("message").contains("Not Found");
    }

    public Boolean isRequiredAuthenticationForGistDeletion(String authToken, String gistId) {
        Response response = deleteUsersGistById(authToken, gistId);
        logger.info(response.body().asString());
        return response.statusCode() == 404 && new JSONObject(response.body().asString())
                .getString("message").contains("Not Found");
    }
}
