package general;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.String.format;
import static managers.ConfigFileManager.*;

public class AbstractApi {

    private static final Logger log = LogManager.getLogger(AbstractApi.class);


    protected Response get(String uri) {
        return RestAssured.get(getDefaultApiUri() + uri);
    }

    protected Response get(String token, String uri) {
        Response response = RestAssured.with().auth().oauth2(token).get(getDefaultApiUri() + uri);
        if (response.statusCode() == 401) {
            throw new IllegalArgumentException(format("Unknown user's token [%s]", token));
        }
        return response;
    }

    protected Response delete(String uri) {
        return RestAssured.delete(getDefaultApiUri() + uri);
    }

    protected Response delete(String token, String uri) {
        Response response = RestAssured.with().auth().oauth2(token).delete(getDefaultApiUri() + uri);
        if (response.statusCode() == 401) {
            throw new IllegalArgumentException(format("Unknown user's token [%s]", token));
        }
        return response;
    }

    protected Response post(String token, String uri, JSONObject json) {
        return RestAssured.with().auth().oauth2(token).
                body(json.toString()).post(getDefaultApiUri() + uri);
    }

    protected Response post(String uri, JSONObject json) {
        return RestAssured.with().
                body(json.toString()).post(getDefaultApiUri() + uri);
    }

    protected Response patch(String token, String uri, JSONObject json) {
        return RestAssured.with().auth().oauth2(token).
                body(json.toString()).patch(getDefaultApiUri() + uri);
    }

    protected Response patch(String uri, JSONObject json) {
        return RestAssured.with().body(json.toString()).patch(getDefaultApiUri() + uri);
    }

    protected List<JSONObject> jsonArrayToList(JSONArray jsonArray) {
        return IntStream.range(0, jsonArray.length())
                .mapToObj(jsonArray::get)
                .map(e -> (JSONObject) e)
                .collect(Collectors.toList());
    }

    public String getUserName(String authToken) {
        if (authToken == null) {
            throw new IllegalArgumentException("User is Anonymous");
        }
        String uri = getPropertyValueByName(CURRENT_USERS_API);
        log.info(uri);
        Response response = get(authToken, uri);
        return new JSONObject(response.body().asString()).getString("login");
    }


}
