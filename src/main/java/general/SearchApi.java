package general;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static managers.ConfigFileManager.SEARCH_API;
import static managers.ConfigFileManager.getPropertyValueByName;

public class SearchApi extends AbstractApi {

    private static final Logger logger = LogManager.getLogger(SearchApi.class);


    public void sendRandomSearchRequest(String authToken) {
        String uri = getPropertyValueByName(SEARCH_API)
                + "/users?q=octocat";
        logger.info(uri);
        if (authToken == null) {
            get(uri);
        } else {
            get(authToken, uri);
        }
    }

    public void sendRandomSearchRequests(String authToken, int amount) {
        for (int i = 0; i < amount; i++) {
            sendRandomSearchRequest(authToken);
        }
    }

}
