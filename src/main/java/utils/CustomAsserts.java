package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class CustomAsserts {
    private static final Logger logger = LogManager.getLogger(CustomAsserts.class);

    public static void assertEventually(long timeout, long pause, Runnable junitAssertion) {
        final long end = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(timeout);
        while (System.currentTimeMillis() <= end) {
            if (tryAssert(pause, junitAssertion)) return;
        }
        junitAssertion.run();
    }

    public static void assertEventually(int attempts, long pause, Runnable junitAssertion) {
        for (; attempts > 0; attempts--) {
            if (tryAssert(pause, junitAssertion)) return;
        }
        junitAssertion.run();
    }

    private static boolean tryAssert(long pause, Runnable junitAssertion) {
        try {
            junitAssertion.run();
            return true;
        } catch (Exception | AssertionError e) {
            logger.debug("Assertion attempt failed: {}", e.getMessage());
        }
        try {
            Thread.sleep(pause);
        } catch (InterruptedException re) {
            throw new RuntimeException(re);
        }
        return false;
    }
}
