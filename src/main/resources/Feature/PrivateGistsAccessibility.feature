Feature: Private Gists Accessibility
  Logged user can create two kinds of gists: public and secret and change their accessibility.
  Anonymous user can read the public gists only.

  Scenario:  The private gist is not displayed for anonymous user
    Given no gists with names "gist_rebels_1" under user "usertest1"
    When User "usertest1" creates a private gist "gist_rebels_1"
    Then the user "usertest1" can see the gist of the target user "usertest1" with gistName "gist_rebels_1"
    Then the user "anonymous" can't see the gist of the target user "usertest1" with gistName "gist_rebels_1"

  Scenario: The public gist is displayed for anonymous user
    Given no gists with names "gist_rebels_2" under user "usertest2"
    When User "usertest2" creates a public gist "gist_rebels_2"
    Then the user "usertest2" can see the gist of the target user "usertest2" with gistName "gist_rebels_2"
    Then the user "anonymous" can see the gist of the target user "usertest2" with gistName "gist_rebels_2"

  Scenario: For anonymous user all public gists are displayed
    Given no gists with names "gist_rebels_1.1,gist_rebels_1.2" under user "usertest1"
    When User "usertest1" creates a public gist "gist_rebels_1.1"
    When User "usertest1" creates a public gist "gist_rebels_1.2"
    Then user "anonymous" can see updated in "10" seconds by method Show All Gists the gists:
      | gist_rebels_1.1 | gist_rebels_1.2 |

  @ignore
  Scenario: For anonymous user the gist is not displayed before changing of the publicity status
    Given a "private" gist "gist_rebels_2" under user "usertest2"
    Then the user "usertest2" can see the gist of the target user "usertest2" with gistName "gist_rebels_2"
    Then the user "anonymous" can't see the gist of the target user "usertest2" with gistName "gist_rebels_2"
    #The next step found a bug on github. More detailed here https://github.com/isaacs/github/issues/329
    When The user "usertest2" make the private gist "gist_rebels_2" public
    #failed because of previous step which does not work
    Then the user "anonymous" can see the gist of the target user "usertest2" with gistName "gist_rebels_2"

  Scenario: For logged user the private gist of another user is not displayed
    Given no gists with names "gist_rebels_3" under user "usertest1"
    When User "usertest1" creates a private gist "gist_rebels_3"
    Then the user "usertest1" can see the gist of the target user "usertest1" with gistName "gist_rebels_3"
    Then the user "usertest2" can't see the gist of the target user "usertest1" with gistName "gist_rebels_3"

  Scenario: For logged user the public gist of another user is displayed
    Given no gists with names "gist_rebels_4" under user "usertest2"
    When User "usertest2" creates a public gist "gist_rebels_4"
    Then the user "usertest2" can see the gist of the target user "usertest2" with gistName "gist_rebels_4"
    Then the user "usertest1" can see the gist of the target user "usertest2" with gistName "gist_rebels_4"

  Scenario: For logged user the public gists are displayed
    Given no gists with names "gist_rebels_5.1,gist_rebels_5.2" under user "usertest1"
    When User "usertest1" creates a public gist "gist_rebels_5.1"
    When User "usertest1" creates a public gist "gist_rebels_5.2"
    Then user "usertest2" can see updated in "10" seconds by method Show All Gists the gists:
      | gist_rebels_5.1 | gist_rebels_5.2 |

  @ignore
  Scenario: For logged user the private gists is not displayed before changing of the publicity status
    Given a "private" gist "gist_rebels_6" under user "usertest2"
    Then the user "usertest2" can see the gist of the target user "usertest2" with gistName "gist_rebels_6"
    Then the user "usertest1" can't see the gist of the target user "usertest2" with gistName "gist_rebels_6"
    #The next step found a bug on github. More detailed here https://github.com/isaacs/github/issues/329
    When The user "usertest2" make the private gist "gist_rebels_6" public
    #failed because of previous step which does not work
    Then the user "usertest1" can see the gist of the target user "usertest2" with gistName "gist_rebels_6"

