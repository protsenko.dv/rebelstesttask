Feature: Gists CRUD
  The users can create, read, update and delete the gists

   ################################ Create ##################################
  Scenario: User successfully creates a gist with 1 symbol
    Given no gists with names "1" under user "usertest1"
    When User "usertest1" creates a private gist "1"
    Then the user "usertest1" can see the gist of the target user "usertest1" with gistName "1"

  Scenario: User successfully creates a gist with 50 symbols
    Given no gists with names "1234567890qwertyuiop1234567890qwertyuiop1234567890" under user "usertest1"
    When User "usertest1" creates a public gist "1234567890qwertyuiop1234567890qwertyuiop1234567890"
    Then the user "usertest1" can see the gist of the target user "usertest1" with gistName "1234567890qwertyuiop1234567890qwertyuiop1234567890"

  Scenario: User successfully creates a gist with empty name. The name is autogenerated
    Given no gists with names "gistfile1.txt" under user "usertest1"
    When User "usertest1" creates a public gist " "
    Then the user "usertest1" can see the gist of the target user "usertest1" with gistName "gistfile1.txt"

  Scenario: User successfully creates a gist without "public" parameter. The gist is private by default.
    Given no gists with names "privatByDefault" under user "usertest1"
    When User "usertest1" creates a gist "privatByDefault" without "public" parameter
    Then the user "usertest1" can see the gist of the target user "usertest1" with gistName "privatByDefault"
    Then the user "usertest2" can't see the gist of the target user "usertest1" with gistName "privatByDefault"

  Scenario: User can't create a gist without "files" parameter. An error returns.
    Then Validation Failed when the user "usertest1" creates a gist without "files" parameter

  Scenario: User can't create a gist without "content" parameter. An error returns.
    Then Validation Failed when the user "usertest1" creates a gist without "content" parameter

  Scenario: User can't create a gist with empty "content" parameter. An error returns.
    Then Validation Failed when the user "usertest1" creates a gist with empty "content" parameter

  Scenario: Anonymous user can't create a gist
    Then the system returns Requires authentication error when user "anonymous" tries create a gist

    ################################ Edit ##################################

  Scenario: User successfully edits a name of the gist 
    Given no gists with names "toEditName" under user "usertest1"
    When User "usertest1" creates a "public" gist with name "toEditName" and saves Id to storage
    Then User "usertest1" successfully edited a name of gist "toEditName" from storage to new gist name "nameSuccessfullyEdited"

  Scenario: User successfully edits a content of the gist
    Given no gists with names "toEditContent" under user "usertest1"
    When User "usertest1" creates a "public" gist with name "toEditContent" and saves Id to storage
    Then User "usertest1" successfully edited a content of gist "toEditContent" from storage to new gist content "contentSuccessfullyEdited"

  Scenario: User successfully change gist's name a to empty name. New name is autogenerated
    Given no gists with names "gistfile1.txt" under user "usertest1"
    When User "usertest1" creates a "public" gist with name "toMakeMeUnnamed" and saves Id to storage
    When User "usertest1" edits a name of gist "toMakeMeUnnamed" from storage to new gist name " "
    Then the user "usertest1" can see the gist of the target user "usertest1" with gistName "gistfile1.txt"

  #I found a bug: during editing of content it's possible create a gist without content. It happens if I run the test below
  @ignore
  Scenario: User can't edit a gist to empty "content" parameter. An error returns.
    Given no gists with names "toEmptyContent" under user "usertest2"
    When User "usertest2" creates a "public" gist with name "toEmptyContent" and saves Id to storage
    Then Validation Failed when the user "usertest2" edits a gist "toEmptyContent" to empty "content" parameter

  Scenario: Anonymous user can't edit a gist
    Given no gists with names "anonymousCantEdit" under user "usertest2"
    When User "usertest2" creates a "public" gist with name "anonymousCantEdit" and saves Id to storage
    Then the system returns Requires authentication error when user "anonymous" tries edit a gist "anonymousCantEdit"

  Scenario: User can't edit an another's gist
    Given no gists with names "anotherCantEdit" under user "usertest2"
    When User "usertest2" creates a "public" gist with name "anotherCantEdit" and saves Id to storage
    Then the system returns Requires authentication error when user "usertest1" tries edit a gist "anotherCantEdit"

    ################################ Delete ##################################
  Scenario: User can delete own private gist
    Given a "private" gist "gistToDeletion" under user "usertest2"
    When User "usertest2" deletes gists with names "gistToDeletion"
    Then the user "usertest2" can't see the gist of the target user "usertest2" with gistName "gistToDeletion"

  Scenario: User can delete own public gists
    Given a "public" gist "gistToDeletionP" under user "usertest1"
    When User "usertest1" deletes gists with names "gistToDeletionP"
    Then the user "usertest1" can't see the gist of the target user "usertest1" with gistName "gistToDeletionP"
    Then the user "anonymous" can't see the gist of the target user "usertest1" with gistName "gistToDeletionP"

  Scenario: Anonymous user can't delete a gist
    Given no gists with names "anonymousCantDelete" under user "usertest1"
    When User "usertest1" creates a "public" gist with name "anonymousCantDelete" and saves Id to storage
    Then the system returns Requires authentication error when user "anonymous" tries delete a gist "anonymousCantDelete"

  Scenario: User can't delete an another's gist
    Given no gists with names "anotherCantDelete" under user "usertest1"
    When User "usertest1" creates a "public" gist with name "anotherCantDelete" and saves Id to storage
    Then the system returns Requires authentication error when user "usertest2" tries delete a gist "anotherCantDelete"
